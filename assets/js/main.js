jQuery(document).ready(function () {
    // toggle sidebar when button clicked
    jQuery('.sidebar-toggle').on('click', function () {
        jQuery('.sidebar').toggleClass('toggled');
    });

    // auto-expand submenu if an item is active
    var active = jQuery('.sidebar .active');

    if (active.length && active.parent('.collapse').length) {
        var parent = active.parent('.collapse');

        parent.prev('a').attr('aria-expanded', true);
        parent.addClass('show');
    }

    jQuery('[data-toggle="tooltip"]').tooltip({
        container: false
    });

    jQuery('.select2').select2();
});
